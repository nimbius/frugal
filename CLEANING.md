### frugal cleaning 

This section covers household cleaning.

###  Cleaning chemicals and media
- source these from discount stores only.
- brushes and rags should be championed over sponges.  exotic designs or packaging should be avoided.
- reusable gloves should replace nitrile or disposable gloves.

### Leather cleaning

Leather goods can be cleaned and deodorized with soap and water, or simply deodorized with white vinegar.
A conditioner such as castor oil should be applied once the leather is dry.

