# The frugal repo.
## DIY recipes and frugal living
In behavioral science, frugality has been defined as the tendency to acquire goods and services in a restrained manner, and resourceful use of already owned economic goods and services, to achieve a longer term goal. 
In other words..."use it up, wear it out, make it do or do without"

this is a collaboration of recipes from Lainchan.org /DIY, expanded to include a primer on frugal living.  contributions should be in plaintext format.  
PDF accepted when necessary. 

### General Guidelines:
- Bread recipes should be in bakers ratios or percentages
- Keep things in metric when possible.
- If it feels like something you'd find Guy Fieri or Paula Deen trying to cook or eat, try to avoid it.
- Avoid recipes that are over-reliant on prepared items.  this is DIY, not Sandra Lee's Kwanzaa Cake...
- no links to specific products.

### why a GIT?
Most websites on this topic are clickbait masquerading as a legitimate ideas for frugal living.
Pay close attention, the first thing most of these blogs/vlogs/sockpuppet theatre articles do is attempt to provide you with a product or service
you can click on to order.  Buying things != frugal living.
GIT does not support sexy marketing tie-ins or branding, so its much easier to quickly convey the form and function of frugality.

### some of this seems time consuming and inefficient.
The goal of this repository is not to make an economic build/buy argument, but to sidestep the predatory and increasingly harmful culture of consumerism driven by large multinational corporations.

### TO DO:
better explanation of some recipes and how to cook them.

# Sections:
[Hygene](HYGENE.md)
[Recipes](recipes/README.md)
[Meal Preparation](recipes/MEALPREP.md)
